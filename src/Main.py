choice = int(input("1. Press '1' to compress and encrypt files.\n2. Press '2' to decrypt and decompress files.\n"))

if choice == 1:
    exec(open("compress.py").read())
    exec(open("Encrypt.py").read())

elif choice == 2:
    exec(open("Decrypt.py").read())
    exec(open("decompress.py").read())

else:
    print("Please select a valid option!")
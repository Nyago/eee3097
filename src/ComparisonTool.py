numLines = 0

def compare(filename1,filename2):
    global numLines
    with open(filename1, 'r') as file1:
        with open(filename2, 'r') as file2:
            same = set(file1).intersection(file2)

    same.discard('\n')

    
    for line in same:
        numLines += 1

file1 = input("file 1:")
file2 = input("file 2:")

compare(file1,file2)
print("Number of similar lines:",numLines)
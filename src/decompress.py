import zlib
import os
import time
def decompress(filename):
    #filename = str(input("Enter name of file to decompress: "))
    tic = time.perf_counter()
    compressed_data = open(filename, 'rb').read()
    decompressed_data = zlib.decompress(compressed_data)

    f = open(filename[:-4], 'wb')
    f.write(decompressed_data)  #write decompressed data to outputfile
    f.close()
    toc = time.perf_counter()
    duration = toc - tic
    #print(f"\ndecompression took: {duration:0.4f} ms\n")
    os.remove(filename)

def decompress_all_files():
    dirs = getAllFiles()
    for file_name in dirs:
        decompress(file_name)

def getAllFiles():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    dirs = []
    for dirName, subdirList, fileList in os.walk(dir_path + "/data"):
        for fname in fileList:
            if (fname != 'data.txt.enc' and str(fname)[-4:] == ".cmp"):
                dirs.append(dirName + "/" + fname)
    return dirs

decompress_all_files()
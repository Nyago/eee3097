import zlib
import os
import time

def compress(filename):
    #filename = str(input("Enter name of file to compress: "))
    tic = time.perf_counter()
    original_data = open(filename, 'rb').read() #read file containing data

    compressed_data = zlib.compress(original_data, level= -1)
    #compressed_data = zlib.compress(original_data, zlib.Z_BEST_COMPRESSION) #compress orginal data

    compress_ratio = (float(len(original_data)) - float(len(compressed_data))) / float(len(original_data))

    #print('Compressed: %d%%' % (100.0 * compress_ratio))
    f = open(filename + ".cmp", 'wb')
    f.write(compressed_data)  #write compressed data to outputfile
    f.close()
    toc = time.perf_counter()
    duration = toc - tic
    #print(f"\nCompression took: {duration:0.4f} ms\n")
    os.remove(filename)

def compress_all_files():
    dirs = getAllFiles()
    for file_name in dirs:
        compress(file_name)

def getAllFiles():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    dirs = []
    for dirName, subdirList, fileList in os.walk(dir_path + "/data"):
        for fname in fileList:
            if (fname != 'data.txt.enc' and fname != '.gitkeep' and str(fname)[-4:] != ".cmp" and str(fname)[-8:] != ".cmp.enc"):
                dirs.append(dirName + "/" + fname)
    return dirs

compress_all_files()